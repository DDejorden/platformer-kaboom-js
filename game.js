kaboom({
    global: true,
    fullscreen: true,
    scale: 2,
    debug: true,
    clearColor: [0, 0, 0, 1]
})


const MOVE_SPEED = 120
const JUMP_FORCE = 360
const BIG_JUMP_FORCE = 550
let CURRENT_JUMP_FORCE = JUMP_FORCE
let isJumping = true
const FALL_DEATH = 400
const ENEMY_SPEED_LEFT = -20
const ENEMY_SPEED_RIGHT = 20
let CURRENT_ENEMY_DIRECTION = ENEMY_SPEED_LEFT

//loads sprites used in the game
loadRoot('https://i.imgur.com/')
loadSprite('coin', 'wbKxhcd.png')
loadSprite('enemy', 'KPO3fR9.png')
loadSprite('brick', 'pogC9x5.png')
loadSprite('block', 'M6rwarW.png')
loadSprite('mario', 'Wb1qfhK.png')
loadSprite('powerUp', '0wMd92p.png')
loadSprite('suprise', 'gesQ1KP.png')
loadSprite('unboxed', 'bdrLpi6.png')
loadSprite('pipe-top-left', 'ReTPiWY.png')
loadSprite('pipe-top-right', 'hj2GK4n.png')
loadSprite('pipe-bottom-left', 'c1cYSbt.png')
loadSprite('pipe-bottom-right', 'nqQ79eI.png')

loadSprite('blue-block', 'fVscIbn.png')
loadSprite('blue-brick', '3e5YRQd.png')
loadSprite('blue-steel', 'gqVoI2b.png')
loadSprite('blue-enemy', 'SvV4ueD.png')
loadSprite('blue-suprise', 'RMqCc1G.png')

//Makes a level
scene("game", ({ level, score }) => {
    layers(['bg', 'obj', 'ui'], 'obj')
    const maps = [
        [
            '                                             ',
            '                                             ',
            '                                             ',
            '                                             ',
            '                                             ',
            '                                             ',
            '             %  =*=%=                        ',
            '                                             ',
            '           -+          -+                    ',
            '           ()  ^       ()                    ',
            '=================================  ==========',
            '=================================  =========='
        ],
        [    
            '@                                             ',
            '@                                             ',
            '@                                             ',
            '@                                             ',
            '@                                             ',
            '@                                             ',
            '@                  ssss                       ',
            '@                                             ',
            '@                           ssss           -+ ',
            '@                       z               z  () ',
            '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',
            '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        ]
    ]

    //Configures wich character is wich sprite.
    const levelCfg = {
        width: 20,
        height: 20,
        '=': [sprite('block'), solid()],
        '$': [sprite('coin'), 'coin'],
        '%': [sprite('suprise'), solid(), 'coin-suprise'],
        '*': [sprite('suprise'), solid(), 'powerUp-suprise'],
        '}': [sprite('unboxed'), solid()],
        '(': [sprite('pipe-bottom-left'), solid(), scale(0.5), 'pipe-bottom'],
        ')': [sprite('pipe-bottom-right'), solid(), scale(0.5), 'pipe-bottom'],
        '-': [sprite('pipe-top-left'), solid(), scale(0.5), 'pipe-top'],
        '+': [sprite('pipe-top-right'), solid(), scale(0.5), 'pipe-top'],
        '^': [sprite('enemy'), solid(), 'dangerous', body()],
        '#': [sprite('powerUp'), solid(), 'mushroom', body()],
        '!': [sprite('blue-block'), solid(), scale(0.5)],
        '@': [sprite('blue-brick'), solid(), scale(0.5)],
        'z': [sprite('blue-enemy'), solid(), scale(0.5), 'dangerous'],
        's': [sprite('blue-suprise'), solid(), scale(0.5), 'coin-suprise'],
        'x': [sprite('blue-steel'), solid(), scale(0.5)],
    }

    const gameLevel = addLevel(maps[level], levelCfg)

    //Makes a piece of text displayed on the UI layer.
    const scoreLabel = add([
        text(score),
        pos(30, 6),
        layer('ui'),
        {
            value: score,
        }
    ])

    add([text('level ' + parseInt(level + 1)), pos(40, 6)])

    function big() {
        let timer = 0
        let isBig = false
        return {
            update() {
                if (isBig) {
                    CURRENT_JUMP_FORCE = BIG_JUMP_FORCE
                    timer -= dt()
                    if (timer <= 0) {
                        this.smallify()
                    }
                }
            },
            isBig() {
                return isBig
            },
            smallify() {
                CURRENT_JUMP_FORCE = JUMP_FORCE
                this.scale = vec2(1)
                timer = 0
                isBig = false
            },
            biggify(time) {
                this.scale = vec2(2)
                timer = time
                isBig = true
            }
        }
    }

    //adds the sprite for the player, and places it in the world/
    const player = add([
        sprite('mario'), solid(),
        pos(30, 0),
        body(),
        big(),
        origin('bot')
    ])

    action('mushroom', (m) => {
        m.move(50, 0)
    })

    //If a player headbumps the coin-suprise block, it spawns a coin above the block (obj.grdPos.sub(0, 1))
    player.on("headbump", (obj) => {
        if (obj.is('coin-suprise')) {
            gameLevel.spawn('$', obj.gridPos.sub(0, 1))
            destroy(obj)
            gameLevel.spawn('}', obj.gridPos.sub(0, 0))
        }
        else if (obj.is('powerUp-suprise')) {
            gameLevel.spawn('#', obj.gridPos.sub(0, 1))
            destroy(obj)
            gameLevel.spawn('}', obj.gridPos.sub(0, 0))
        }
    })

    player.collides('mushroom', (m) => {
        destroy(m)
        player.biggify(6)
    })

    player.collides('coin', (c) => {
        destroy(c)
        scoreLabel.value++
        scoreLabel.text = scoreLabel.value
    })

    action('dangerous', (d) => {
        d.move(CURRENT_ENEMY_DIRECTION, 0)
    })

    player.collides('dangerous', (d) => {
        if (isJumping) {
            destroy(d)
        } else {
            go('lose', { score: scoreLabel.value })
        }
    })

    collides('dangerous', 'pipe-bottom', () => {
        if (CURRENT_ENEMY_DIRECTION === ENEMY_SPEED_LEFT) {
            CURRENT_ENEMY_DIRECTION = ENEMY_SPEED_RIGHT
            console.log("GO RIGHT!")
        } else if(CURRENT_ENEMY_DIRECTION === ENEMY_SPEED_RIGHT){
            CURRENT_ENEMY_DIRECTION = ENEMY_SPEED_LEFT
            console.log("GO LEFT!")
        }
    })

    player.action(() => {
        camPos(player.pos)
        if (player.pos.y >= FALL_DEATH) {
            go('lose', { score: scoreLabel.value })
        }
    })

    player.collides('pipe-top', () => {
        keyPress('down', () => {
            go ('game', {
                level: (level + 1) % maps.length, 
                score: scoreLabel.value
             })
        })
    })

    // Listens to input from keyboard and applies force to the player so it moves.
    keyDown('left', () => {
        player.move(-MOVE_SPEED, 0)
    })

    keyDown('right', () => {
        player.move(MOVE_SPEED, 0)
    })

    player.action(() => {
        if (player.grounded()) {
            isJumping = false
        }
    })

    keyPress('space', () => {
        if (player.grounded()) {
            isJumping = true
            player.jump(CURRENT_JUMP_FORCE)
        }
    })

})

scene('lose', ({ score }) => {
    add([text(score, 32), origin('center'), pos(width() / 2, height() / 2)])
})

start("game", { level: 0, score: 0 })